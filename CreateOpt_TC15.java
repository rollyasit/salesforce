import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CreateOpt_TC15 {
	
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement opportunity=driver.findElement(By.xpath("//a[contains(text(),'Opportunities')]"));
		opportunity.click();	
		
		if (driver.getCurrentUrl().equals("https://na132.salesforce.com/006/o"))
			loggers.log(LogStatus.INFO, "Opportunity page is loaded successfully");
		else
			loggers.log(LogStatus.INFO, "Opportunity page is not loaded successfully");
		
		WebElement opportunityList=driver.findElement(By.xpath("//select[@id='fcf']"));
		opportunityList.click();
		
		boolean allOpprfound=false;
		boolean closeNext=false;
		boolean closeThis=false;
		boolean myOppor=false;
		boolean newThis=false;
		boolean opporPipe=false;
		boolean recentlyView=false;
		boolean won=false;
		
		List<WebElement> dropDownElement = driver.findElements(By.xpath("//select[@id='fcf']"));
		for(WebElement value : dropDownElement) 
		{
			if(value.getText().contains("All Opportunities") )
		   {
		    	allOpprfound=true;
		       // break;
		    }
			if (value.getText().contains("Closing Next Month")) 
				{
				closeNext=true;
				}
			if (value.getText().contains("Closing This Month")) 
			{
				closeThis=true;
			}
			if (value.getText().contains("My Opportunities"))
			{
				myOppor=true;
			}
			if (value.getText().contains("New This Week")) 
			{
				newThis=true;
			}
			if (value.getText().contains("Opportunity Pipeline")) 
			{
				opporPipe=true;
			}
			if (value.getText().contains("Recently Viewed Opportunities"))
			{
				recentlyView=true;
			}	
			if (value.getText().contains("Won"))
			{
				won=true;
			}
		}
		if(allOpprfound && closeNext && closeThis && myOppor && newThis && opporPipe && recentlyView && won)
			    loggers.log(LogStatus.INFO, "Drop down with All Oppotunities, Closing Next Month, Closing This Month, My Opportunities,New This Week, Recently Viewed Opportunities, Won is loaded successfully");			
			else
			    loggers.log(LogStatus.INFO, "Drop down with All Oppotunities, Closing Next Month, Closing This Month, My Opportunities,New This Week, Recently Viewed Opportunities, Won is not loaded successfully");					
		
		reports.endTest(loggers);
		reports.flush();	
		
	}

}
