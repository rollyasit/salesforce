import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CreateOpty_TC19 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement opportunity=driver.findElement(By.xpath("//a[contains(text(),'Opportunities')]"));
		opportunity.click();	
		
		if (driver.getCurrentUrl().equals("https://na132.salesforce.com/006/o"))
			loggers.log(LogStatus.INFO, "Opportunity page is loaded successfully");
		else
			loggers.log(LogStatus.INFO, "Opportunity page is not loaded successfully");
		
		WebElement interval=driver.findElement(By.xpath("//select[@id='quarter_q']"));
		Select intervalCombp=new Select(interval);
		intervalCombp.selectByVisibleText("Current FQ");
		Thread.sleep(5000);
		
		WebElement include=driver.findElement(By.xpath("//select[@id='open']"));
		Select includeCombp=new Select(include);
		includeCombp.selectByVisibleText("Open Opportunities");
		Thread.sleep(5000);
		
		WebElement runReport=driver.findElement(By.xpath("//table[contains(@class,'opportunitySummary')]//input[contains(@name,'go')]"));
		runReport.click();
		Thread.sleep(5000);
		
		reports.endTest(loggers);
		reports.flush();

	}

}
