
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.server.handler.SwitchToFrame;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CreateAccount_TC13 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement account =driver.findElement(By.xpath("//a[contains(text(),'Accounts')]"));
		account.click();
		Thread.sleep(5000);
		
		WebElement mergeAcct =driver.findElement(By.xpath("//a[contains(text(),'Merge Accounts')]"));
		mergeAcct.click();
		Thread.sleep(5000);

		WebElement mergeMyAcct=driver.findElement(By.xpath("//input[@id='srch']"));
		mergeMyAcct.clear();
		mergeMyAcct.sendKeys("Get");
		Thread.sleep(5000);
		
		WebElement findAcct=driver.findElement(By.xpath("//input[@name='srchbutton']"));
		findAcct.click();
		Thread.sleep(5000);

		WebElement table1=driver.findElement(By.xpath("//*[@id=\"stageForm\"]/div/div[2]/div[4]/div/div[1]/div/div[2]/table"));
		List<WebElement> rows=table1.findElements(By.tagName("tr"));
		Thread.sleep(5000);
		System.out.println(rows.size());
		String acctName1 = null, acctName2 = null;
		
		if (rows.size()>=3)
		{
			WebElement acct1=driver.findElement(By.xpath("//input[@id='cid0']"));
			if (!acct1.isSelected())
				acct1.click();
			WebElement acct2=driver.findElement(By.xpath("//input[@id='cid1']"));
			if (!acct2.isSelected())
			acct2.click();
			
			acctName1=driver.findElement(By.xpath("//*[@id=\"stageForm\"]/div/div[2]/div[4]/div/div[1]/div/div[2]/table/tbody/tr[2]/td[1]")).getText();
			acctName2=driver.findElement(By.xpath("//*[@id=\"stageForm\"]/div/div[2]/div[4]/div/div[1]/div/div[2]/table/tbody/tr[3]/td[1]")).getText();			
		//System.out.println(acctName1 + "  " + acctName2);
		}
		WebElement nextBtn=driver.findElement(By.xpath("//div[@class='pbBottomButtons']//input[@name='goNext']"));
		nextBtn.click();
		Thread.sleep(5000);
		
		WebElement mergeTable=driver.findElement(By.xpath("//*[@id=\"stageForm\"]/div/div[2]/div[4]/table"));
		List <WebElement> rowS=mergeTable.findElements(By.tagName("tr"));
		
		List <WebElement> cols=rowS.get(0).findElements(By.tagName("th"));
		System.out.println(cols.size());
		
		String acctNameDisplay1=cols.get(0).getText();
		String acctNameDisplay2=cols.get(1).getText();
		//System.out.println(acctNameDisplay1 +"  " +acctNameDisplay2);

		System.out.println();
		if ((acctNameDisplay1.contains(acctName1))&& (acctNameDisplay2.contains(acctName2)) )
			loggers.log(LogStatus.INFO, "Merge acct page is displayed with selected acct name successfully");		
		else
			loggers.log(LogStatus.FAIL, "Merge acct page is not displayed with selected acct name");		
		
		WebElement mergebtn=driver.findElement(By.xpath("//div[@class='pbBottomButtons']//input[@name='save']"));
			mergebtn.click();
			Thread.sleep(5000);
			Alert alert=driver.switchTo().alert();
			alert.accept();
		
		if (driver.getCurrentUrl().contains("https://na132.salesforce.com/001/o"))
			loggers.log(LogStatus.INFO, "Acct page is displayed");
		
		WebElement acctTable=driver.findElement(By.xpath("//*[@id=\"bodyCell\"]/div[3]/div[1]/div/div[2]/table"));
		List <WebElement> tableForAcct=acctTable.findElements(By.tagName("tr"));
		if (tableForAcct.size()>2)
		{
			String updateAcct=driver.findElement(By.xpath("//*[@id=\"bodyCell\"]/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th")).getText();
			if (updateAcct.equals(acctName1))
				loggers.log(LogStatus.INFO, "In recently viwed view merge acct is displayed");
		}
		Thread.sleep(5000);
		
		reports.endTest(loggers);
		reports.flush();  
	}

}
