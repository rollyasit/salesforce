import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CreateAccount_TC14 {
	
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement account =driver.findElement(By.xpath("//a[contains(text(),'Accounts')]"));
		account.click();
		Thread.sleep(5000);
		
		WebElement lastAct=driver.findElement(By.xpath("//a[contains(text(),'Accounts with last activity > 30 days')]"));
		lastAct.click();

		WebElement lastSaved=driver.findElement(By.xpath("//h2[@class='pageDescription']"));
		if (lastSaved.getText().contains("Unsaved Report"))
			loggers.log(LogStatus.INFO, "Unsaved Report page is displayed");		
		else
			loggers.log(LogStatus.INFO, "Unsaved Report page is not displayed");	
		
		WebElement dateField=driver.findElement(By.xpath("//input[@id='ext-gen20']"));
		dateField.click();
		
		WebElement createDate =driver.findElement(By.xpath("//div[contains(text(),'Created Date')]"));
		createDate.click();
		
		WebElement fromDt=driver.findElement(By.xpath("//input[@id='ext-comp-1042']"));
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy ");
		Date date = new Date();
		
		fromDt.sendKeys(dateFormat.format(date));
		fromDt.click();
		Thread.sleep(5000);
		
		WebElement toDt=driver.findElement(By.xpath("//input[@id='ext-comp-1045']"));
		toDt.clear();
		toDt.sendKeys(dateFormat.format(date));
		toDt.click();
		//toDt.submit();
		new Actions(driver).moveToElement(fromDt).click().perform();
		Thread.sleep(5000);
		
		WebElement save=driver.findElement(By.xpath("//button[@id='ext-gen49']"));
		save.click();
		Thread.sleep(5000);
		
		WebElement reportName=driver.findElement(By.xpath("//input[@id='saveReportDlg_reportNameField']"));
		reportName.sendKeys("MyReport");
		Thread.sleep(5000);
		
		WebElement reportUniName=driver.findElement(By.xpath("//input[@id='saveReportDlg_DeveloperName']"));
		reportUniName.click();
		Thread.sleep(5000);
		reportUniName.clear();
		reportUniName.sendKeys("MyUniqueReport");
		Thread.sleep(5000);
		
		WebElement saveAndRun=driver.findElement(By.xpath("//*[@id=\"dlgSaveAndRun\"]/tbody/tr[2]/td[2]/em"));
		saveAndRun.click();
		Thread.sleep(5000);	
		
		reports.endTest(loggers);
		reports.flush();  
	}

}
