import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class SalesForceLogin {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("Login Error Message - 1");
		LoginErrorMessage();	
		 
	//********************************************************	

		//CheckRemeberMe();
		loggers=reports.startTest("Forgot Password- 4 A");
		ForgotPassword4A();
		
		reports.endTest(loggers);
		 reports.flush();
		 		 
	}
	public static void LoginErrorMessage() throws InterruptedException
	{
		WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
		userName.sendKeys("rollyrastogi-nxmg@force.com");
		if (userName.getText()!=null)
			loggers.log(LogStatus.INFO, "Username enetered successfully");		
		else
			loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		Thread.sleep(2000);
		passWord.clear();
		
		if (passWord.getText()==null)
			loggers.log(LogStatus.INFO, "Password field is cleared successfully.");		
		else
			loggers.log(LogStatus.INFO, "Password field is not cleared.");
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		
		WebElement errorMsg1=driver.findElement(By.xpath("//div[@id='error']"));
		
		 if (errorMsg1.getText().equals("Please enter your password."))
			 loggers.log(LogStatus.PASS, "Test: Navigate to SFDC is passed.");
		 else
			 loggers.log(LogStatus.FAIL, "Test: Navigate to SFDC is failed.");		
	}
	
	public static void CheckRemeberMe() throws InterruptedException
	{
		 loggers=reports.startTest("Check RemeberMe - 3");

		 WebElement userName3=driver.findElement(By.xpath("//input[@id='username']"));
			userName3.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName3.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord3=driver.findElement(By.xpath("//input[@id='password']"));
		passWord3.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);
		
		WebElement chkbox=driver.findElement(By.xpath("//input[@id='rememberUn']"));
		chkbox.click();
		loggers.log(LogStatus.INFO, "Checked 'Remember me' checked box successfully");
		Thread.sleep(2000);
		
		WebElement logIn3=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn3.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		
		
		//WebElement userIcon=driver.findElement(By.xpath("//div[@class='profileTrigger branding-user-profile bgimg slds-avatar slds-avatar_profile-image-small circular forceEntityIcon']"));
		//userIcon.click();
		//Thread.sleep(6000);
		
		//loggers.log(LogStatus.INFO, "Clicked on user menu");
		
		//WebElement logOut=driver.findElement(By.xpath("//a[contains(text(),'Log Out')]"));
	//	logOut.click();
	//	loggers.log(LogStatus.INFO, "Clicked on Log out");	
	}
	public static void ForgotPassword4A() throws InterruptedException
	{
		WebElement forgotPwd=driver.findElement(By.xpath("//a[@id='forgot_password_link']"));
		forgotPwd.click();
		
		if (driver.getCurrentUrl().equals("https://login.salesforce.com/secur/forgotpassword.jsp?locale=us"))
			loggers.log(LogStatus.INFO, "Forgot password page is loaded");
		else
			loggers.log(LogStatus.FAIL, "Forgot password page is not loaded");
		Thread.sleep(2000);
		
		WebElement ForgotUsername=driver.findElement(By.xpath("//input[@id='un']"));
		ForgotUsername.sendKeys("rollyrastogi-nxmg@force.com");
		WebElement continueButton=driver.findElement(By.xpath("//input[@id='continue']"));
		continueButton.click();
		Thread.sleep(2000);
		
		WebElement pwdMsg=driver.findElement(By.xpath("//p[contains(text(),'We�ve sent you an email with a link to finish rese')]"));
		if (pwdMsg.getText().equals("We�ve sent you an email with a link to finish resetting your password."))
			loggers.log(LogStatus.INFO, "Email has been sent to ur mail id");
		else 
			loggers.log(LogStatus.INFO, "email is not send");
	}

}
