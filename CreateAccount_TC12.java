import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CreateAccount_TC12 {
	
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
				
		WebElement userMenu=driver.findElement(By.xpath("//div[@id='userNavButton']"));
		if (userMenu.isDisplayed())
			loggers.log(LogStatus.INFO, "User menu is available");
		else
			loggers.log(LogStatus.INFO, "User menu is not available");
		
		userMenu.click();
		Thread.sleep(2000);	

		Boolean myProfilefound = false;
		Boolean mySetting = false;
		Boolean devConsole = false;
		Boolean logOut = false;
		
		List<WebElement> dropDownElement = driver.findElements(By.className("mbrMenuItems"));
		for(WebElement value : dropDownElement) 
		{
		   //System.out.println(value.getText());
			if(value.getText().contains("My Profile") )
		   {
		    	myProfilefound=true;
		       // break;
		    }
			if (value.getText().contains("My Settings")) 
				{
				mySetting=true;
				}
			if (value.getText().contains("Developer Console")) 
			{
				devConsole=true;
			}
			if (value.getText().contains("Logout"))
			{
				logOut=true;
			}		
		}
		
		if(myProfilefound && mySetting && devConsole && logOut) {
		    loggers.log(LogStatus.INFO, "Drop Down with User Profile, My Settings, Developer Console and Logout is displayed successfully");
		}
		else
		{
			 loggers.log(LogStatus.FAIL, "Drop Down with User Profile, My Settings, Developer Console and Logout is not displayed ");	
		}
		
		WebElement account =driver.findElement(By.xpath("//a[contains(text(),'Accounts')]"));
		account.click();
		Thread.sleep(5000);
		
		WebElement view =driver.findElement(By.xpath("//select[@id='fcf']"));
		Select viewList=new Select(view);
		viewList.selectByIndex(1);
		Thread.sleep(5000);
	
		String viewName=viewList.getOptions().get(1).getText();
		WebElement edit=driver.findElement(By.xpath("//span[@class='fFooter']//a[contains(text(),'Edit')]"));
		edit.click();
		Thread.sleep(5000);
		
		WebElement viewNameTxt= driver.findElement(By.xpath("//input[@id='fname']"));
		if (viewNameTxt.getAttribute("value").equals(viewName))
			loggers.log(LogStatus.INFO, "View name is displayed in edit page");
		else
			loggers.log(LogStatus.INFO, "View name is not displayed in edit page");
		
		String newUpdatedName="Juhi";
		viewNameTxt.clear();
		viewNameTxt.sendKeys(newUpdatedName);
		Thread.sleep(5000);
		
		WebElement filterAcct=driver.findElement(By.xpath("//select[@id='fcol1']"));
		Select filterAcctDrop=new Select(filterAcct);
		filterAcctDrop.selectByIndex(1);		
		Thread.sleep(5000);
		
		WebElement filterOpt=driver.findElement(By.xpath("//select[@id='fop1']"));
		Select filterOptDrop=new Select(filterOpt);
		filterOptDrop.selectByIndex(5);
		Thread.sleep(5000);
		
		WebElement filterValue=driver.findElement(By.xpath("//input[@id='fval1']"));
		filterValue.clear();
		filterValue.sendKeys("B");
		Thread.sleep(5000);
		
		WebElement availField=driver.findElement(By.xpath("//select[@id='colselector_select_0']"));
		Select availFieldDrop=new Select(availField);
		availFieldDrop.selectByIndex(availFieldDrop.getOptions().size()-1);
		String availFieldText=availFieldDrop.getFirstSelectedOption().getText();
		Thread.sleep(5000);
		
		WebElement add=driver.findElement(By.xpath("//img[@class='rightArrowIcon']"));
		add.click();
		Thread.sleep(5000);
		
		WebElement selectField=driver.findElement(By.xpath("//select[@id='colselector_select_1']"));
		Select selectFieldDrop=new Select(selectField);
		int selectFieldSize=selectFieldDrop.getOptions().size();
		System.out.println(selectFieldSize);
		Thread.sleep(5000);
		
		WebElement savebtn=driver.findElement(By.xpath("//div[@class='pbHeader']//input[@name='save']"));
		savebtn.click();
		Thread.sleep(5000);
		
		WebElement viewListUpdated=driver.findElement(By.name("fcf"));
		Select viewAcctList=new Select(viewListUpdated);
		if (viewAcctList.getFirstSelectedOption().getText().equals(newUpdatedName))
			loggers.log(LogStatus.INFO, "View is updated successfully");
		else
			loggers.log(LogStatus.INFO, "View is not updated successfully");
		
		List <WebElement> listcol=driver.findElements(By.xpath("//*[@id=\"ext-gen16\"]/div/table/thead/tr/td"));

		if ((listcol.size())==(selectFieldSize+3))
		{
			if (listcol.get(listcol.size()-1).getText().equals(availFieldText))
				loggers.log(LogStatus.INFO, "View have last activity column added successfully");
			else
				loggers.log(LogStatus.FAIL, "View doesnt have last activity column added");
		}

		String AcctName;
		List <WebElement> listrow=driver.findElements(By.xpath("//*[@id=\"ext-gen12\"]/div[1]/table/tbody/tr/td[4]"));
		System.out.println(listrow.size());	
		for (int i=1; i<=listrow.size() ; i++)	
		{
			AcctName=driver.findElement(By.xpath("//*[@id=\"ext-gen12\"]/div[1]/table/tbody/tr[" +i+ "]/td[4]")).getText();
			System.out.println(AcctName);
		}
		reports.endTest(loggers);
		reports.flush();		

	}

}
