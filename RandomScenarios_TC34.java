import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class RandomScenarios_TC34 {
	
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		Login();
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement homeBtn=driver.findElement(By.xpath("//a[@class='brandPrimaryFgr']"));
		homeBtn.click();
		Thread.sleep(3000);
		if (driver.getCurrentUrl().equals("https://na132.salesforce.com/home/home.jsp"))
			loggers.log(LogStatus.INFO, "Home page is displayed");
		else
			loggers.log(LogStatus.FAIL, "Home page is not displayed");

		WebElement userName=driver.findElement(By.xpath("//h1[@class='currentStatusUserName']//a[contains(text(),'Bunty Shukla')]"));
		String userFullName=userName.getText().toString();
		userName.click();
		
		WebElement editProfile=driver.findElement(By.xpath("//img[@class='recImage user']"));
		editProfile.click();
		Thread.sleep(5000);

		driver.switchTo().frame(driver.findElement(By.id("aboutMeContentId")));

		WebElement contact=driver.findElement(By.xpath("//a[contains(text(),'Contact')]"));
		Actions cont=new Actions(driver);
		cont.moveToElement(contact).click().perform();
		Thread.sleep(5000);
		
		reports.endTest(loggers);
		reports.flush();
	}
	public static void Login() throws InterruptedException
	{
		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			
		 	userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
	}
}
