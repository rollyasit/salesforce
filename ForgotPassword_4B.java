import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ForgotPassword_4B {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("Forgot Password-4B");
		
		WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
		userName.sendKeys("123");
		if (userName.getText()!=null)
			loggers.log(LogStatus.INFO, "Wrong username enetered successfully");		
		else
			loggers.log(LogStatus.INFO, "Wrong username is not enetered successfully");		
		
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("22131");	
		loggers.log(LogStatus.INFO, "Entered wrong password successfully");
		Thread.sleep(2000);
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		
		WebElement errorMsg=driver.findElement(By.xpath("//div[@id='error']"));
		if (errorMsg.getText().equals("Please check your username and password. If you still can't log in, contact your Salesforce administrator."))
			loggers.log(LogStatus.INFO, "Error message is displayed");
		else
			loggers.log(LogStatus.FAIL, "Error message is not displayed");
		reports.endTest(loggers);
		reports.flush();
	}

}
