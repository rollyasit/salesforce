import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CreateAccount_TC10 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
				
		WebElement userMenu=driver.findElement(By.xpath("//div[@id='userNavButton']"));
		if (userMenu.isDisplayed())
			loggers.log(LogStatus.INFO, "User menu is available");
		else
			loggers.log(LogStatus.INFO, "User menu is not available");
		
		userMenu.click();
		Thread.sleep(2000);	

/*		Boolean myProfilefound = false;
		Boolean mySetting = false;
		Boolean devConsole = false;
		Boolean logOut = false;
		
		List<WebElement> dropDownElement = driver.findElements(By.className("mbrMenuItems"));
		for(WebElement value : dropDownElement) 
		{
		   //System.out.println(value.getText());
			if(value.getText().contains("My Profile") )
		   {
		    	myProfilefound=true;
		       // break;
		    }
			if (value.getText().contains("My Settings")) 
				{
				mySetting=true;
				}
			if (value.getText().contains("Developer Console")) 
			{
				devConsole=true;
			}
			if (value.getText().contains("Logout"))
			{
				logOut=true;
			}		
		}
		
		if(myProfilefound && mySetting && devConsole && logOut) {
		    loggers.log(LogStatus.INFO, "Drop Down with User Profile, My Settings, Developer Console and Logout is displayed successfully");
		}
		else
		{
			 loggers.log(LogStatus.FAIL, "Drop Down with User Profile, My Settings, Developer Console and Logout is not displayed ");	
		}
*/		
		WebElement account =driver.findElement(By.xpath("//a[contains(text(),'Accounts')]"));
		account.click();
		
		WebElement newAcct=driver.findElement(By.xpath("//div[@id='createNewButton']"));
		newAcct.click();
		
		WebElement acct=driver.findElement(By.xpath("//a[@class='accountMru menuButtonMenuLink']"));
		acct.click();
		String newName="RRg";
		WebElement acctName=driver.findElement(By.xpath("//input[@id='acc2']"));
		acctName.sendKeys(newName);
		
		WebElement savebtn=driver.findElement(By.xpath("//td[@id='topButtonRow']//input[@name='save']"));
		savebtn.click();
		
		WebElement newAcctName=driver.findElement(By.xpath("//h2[@class='topName']"));
		if (newAcctName.getText().toString().equals(newName))
			loggers.log(LogStatus.INFO, "New acct name is updated successfully");
		else
			loggers.log(LogStatus.INFO, "New acct name is not updated successfully");
		
		reports.endTest(loggers);
		reports.flush(); 
	}

}
