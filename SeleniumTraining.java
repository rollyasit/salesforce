import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class SeleniumTraining {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Selenium Training");
		
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		WebDriver driver;
		driver=new ChromeDriver();
		driver.get("https://selenium-prd.firebaseapp.com/");
		
		Thread.sleep(5000);
		
		WebElement userName=driver.findElement(By.xpath("//input[@id='email_field']"));
		userName.sendKeys("admin123@gmail.com");
		
		WebElement userPassword=driver.findElement(By.id("password_field"));
		userPassword.sendKeys("admin123");
		
		WebElement userLoginButton=driver.findElement(By.xpath("//button[contains(text(),'Login to Account')]"));
		userLoginButton.click();
		
		Thread.sleep(5000);
		WebElement homeTab=driver.findElement(By.xpath("//a[contains(text(),'Home')]"));
		homeTab.click(); 
		
		WebElement name=driver.findElement(By.id("name"));
		name.sendKeys("Rolly");
		
		WebElement fatherName=driver.findElement(By.id("lname"));
		fatherName.sendKeys("Nirdesh Kumar");
		
		WebElement postalAdd=driver.findElement(By.id("postaladdress"));
		postalAdd.sendKeys("Bronocs Pwky");		
		
		WebElement personalAdd=driver.findElement(By.id("personaladdress"));
		personalAdd.sendKeys("Joplin Ct");
		
		WebElement femaleRadioButton=driver.findElement(By.xpath("//span[2]//input[1]"));
		femaleRadioButton.click();
		
		//WebElement maleRadioButton=driver.findElement(By.xpath("//span[1]//input[1]"));
		//maleRadioButton.click();
		
		WebElement city=driver.findElement(By.id("city"));
		Select cityList = new Select(city);   
		cityList.selectByVisibleText("GOA");
		
		WebElement course=driver.findElement(By.id("course"));
		Select courseList=new Select(course);
		courseList.selectByValue("mca");
		
		WebElement district=driver.findElement(By.xpath("//select[@id='district']"));
		Select districtList=new Select(district);
		districtList.selectByIndex(1);
		
		WebElement state=driver.findElement(By.xpath("//select[@id='state']"));
		Select stateList=new Select(state);
		stateList.selectByIndex(2);
		
		WebElement pinCode=driver.findElement(By.id("pincode"));
		pinCode.sendKeys("80111");
		
		WebElement emailId=driver.findElement(By.id("emailid"));
		emailId.sendKeys("xyz@.gmail.com");
		
		Thread.sleep(3000);		
		
		WebElement submit=driver.findElement(By.xpath("//button[@class='bootbutton']"));
		submit.click();
	
		//***********************Switch to*******************************
		
		WebElement switchTo=driver.findElement(By.xpath("//button[contains(text(),'Switch To')]"));
		Actions switchTolist=new Actions(driver);
		switchTolist.moveToElement(switchTo).build().perform();

		Thread.sleep(1000);

		WebElement alert=driver.findElement(By.xpath("//a[contains(text(),'Alert')]"));
		switchTolist.moveToElement(alert).build().perform();
		Thread.sleep(1000);
		alert.click();
		
		WebElement windowAlert=driver.findElement(By.xpath("//button[contains(text(),'Window Alert')]"));
		windowAlert.click();
		
		Alert alertWindow=driver.switchTo().alert();
		String alertMessage=alertWindow.getText();
		System.out.println(alertMessage);
		
		Thread.sleep(1000);	
		if(alertMessage.equals("Hello! I am an alert box!"))
		{
			alertWindow.accept();		
		}

		WebElement promtAlert=driver.findElement(By.xpath("//button[contains(text(),'Promt Alert')]"));
		promtAlert.click();
		Alert alertWindow1=driver.switchTo().alert();

		//alertMessage=alertWindow1.getText();
		Thread.sleep(3000);
		alertWindow1.sendKeys("Rolly");
		
		
		alertWindow1.accept();
		
		WebElement promtmsg=driver.findElement(By.xpath("//p[@id='Selenium']"));
		System.out.println(promtmsg.getText());
		
		
		WebElement switchToTab=driver.findElement(By.xpath("//button[contains(text(),'Switch To')]"));
		switchToTab.click();
		Thread.sleep(5000);
		
		WebElement windows=driver.findElement(By.xpath("//a[contains(text(),'Windows')]"));
		windows.click();
		Thread.sleep(5000);
		
		WebElement newTab=driver.findElement(By.xpath("//a[1]//button[1]"));
		newTab.click();		
		Thread.sleep(3000);
		
		String primaryWindow=driver.getWindowHandle();
		for(String handle:driver.getWindowHandles())
		{
			System.out.println("No of handles: " + handle);
			driver.switchTo().window(handle);

		}
		driver.findElement(By.name("q")).sendKeys("Selenium");
		Thread.sleep(1000);
		driver.switchTo().window(primaryWindow);
		Thread.sleep(1000);
		//WebElement windonewWindow=driver.findElement(By.xpath("//a[2]//button[1]"));
		//windonewWindow.click();
		
		WebElement interactions=driver.findElement(By.xpath("//button[contains(text(),'Intractions')]"));
		interactions.click();
		
		WebElement mouseHover=driver.findElement(By.xpath("//a[contains(text(),'Mouse Hover')]"));
		mouseHover.click();
		Thread.sleep(5000);
		
		WebElement mouse=driver.findElement(By.xpath("//button[contains(text(),'mousehover')]"));
		Actions act=new Actions(driver);
		act.moveToElement(mouse).build().perform();
		Thread.sleep(5000);
		
		WebElement tabs=driver.findElement(By.xpath("//div[@class='dropdown login']//a[contains(text(),'Tabs')]"));
		act.moveToElement(tabs).build().perform();
		Thread.sleep(5000);
		
		//Drag and Drop	---------------------
		
		WebElement interaction=driver.findElement(By.xpath("//button[contains(text(),'Intractions')]"));
		interaction.click();
		Thread.sleep(3000);
		
		WebElement dragDrop=driver.findElement(By.xpath("//a[contains(text(),'Drag & Drop')]"));
		dragDrop.click();
		Thread.sleep(3000);
		
		WebElement image=driver.findElement(By.id("drag1"));
		WebElement dropLeft=driver.findElement(By.xpath("//div[@class='container']//div[1]"));
		WebElement dropRight=driver.findElement(By.xpath("//div[@class='container']//div[2]"));
		
		Actions dragDropImage=new Actions(driver);		
		dragDropImage.clickAndHold(image).moveToElement(dropLeft).release().build().perform();		
		Thread.sleep(5000);

		


	}

}
