import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Contacts_TC26 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		Login();
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement contact=driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		contact.click();
		
		if (driver.getCurrentUrl().equals("https://na132.salesforce.com/003/o"))
			loggers.log(LogStatus.INFO, "Contact page is loaded successfully");		
		else
			loggers.log(LogStatus.INFO, "Contact page is not loaded successfully");
		Thread.sleep(2000);
		
		WebElement createNewView=driver.findElement(By.xpath("//a[contains(text(),'Create New View')]"));
		createNewView.click();
		Thread.sleep(5000);
		if (driver.getCurrentUrl().equals("https://na132.salesforce.com/ui/list/FilterEditPage?ftype=c&retURL=%2F003&cancelURL=%2F003%2Foo"))
			loggers.log(LogStatus.INFO, "New view page is loaded successfully");		
		else
			loggers.log(LogStatus.INFO, "New view page is not loaded successfully");
		
		WebElement viewName=driver.findElement(By.xpath("//input[@id='fname']"));
		String viewString="Deepa";
		viewName.sendKeys(viewString);	
		loggers.log(LogStatus.INFO, "View name is entered successfully");
		Thread.sleep(5000);
		
		WebElement viewUniName=driver.findElement(By.xpath("//input[@id='devname']"));
		viewUniName.click();
		Thread.sleep(5000);
		viewUniName.clear();

		viewUniName.sendKeys("UniqueDeepa");	
		loggers.log(LogStatus.INFO, "Unique View name is entered successfully");
		Thread.sleep(5000);
		
		WebElement saveBtn=driver.findElement(By.xpath("//div[@class='pbHeader']//input[@name='save']"));
		saveBtn.click();		
		Thread.sleep(5000);
			
		//WebElement viewNameCombo=driver.findElement(By.name("fcf]"));
	//	Select viewCombo=new Select(viewName);
	//	if (viewCombo.getFirstSelectedOption().getText().contains(viewString))
	//		loggers.log(LogStatus.INFO, "Created view name is displayed in view drop down successfully");
	//	else
	//		loggers.log(LogStatus.INFO, "Created view name is not displayed in view drop down successfully");
		
		reports.endTest(loggers);
		reports.flush();	
		
	}
	public static void Login() throws InterruptedException
	{
		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			
		 	userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
	}
}
