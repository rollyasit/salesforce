import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Leads_TC24 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement leads=driver.findElement(By.xpath("//a[contains(text(),'Leads')]"));
		leads.click();
		Thread.sleep(5000);	
		
		if(driver.getCurrentUrl().contains("https://na132.salesforce.com/00Q/o"))
			loggers.log(LogStatus.INFO, "Lead home page is opened successfully");
		else
			loggers.log(LogStatus.FAIL, "Lead home page is not opened successfully");
		
		WebElement newBtn=driver.findElement(By.xpath("//input[@name='new']"));
		newBtn.click();
		Thread.sleep(5000);
		
		if(driver.getCurrentUrl().contains("https://na132.salesforce.com/00Q/e?retURL=%2F00Q%2Fo"))
			loggers.log(LogStatus.INFO, "New Lead creation page is opened successfully");
		else
			loggers.log(LogStatus.FAIL, "New Lead creation page is not opened successfully");
		
		
		WebElement lastName=driver.findElement(By.xpath("//input[@id='name_lastlea2']"));
		lastName.clear();
		lastName.sendKeys("ABCD");
		Thread.sleep(5000);
		
		WebElement compName=driver.findElement(By.xpath("//input[@id='lea3']"));
		compName.clear();
		compName.sendKeys("ABCD");	
		Thread.sleep(5000);
		
		WebElement saveBtn=driver.findElement(By.xpath("//td[@id='bottomButtonRow']//input[@name='save']"));
		saveBtn.click();		
		Thread.sleep(5000);
		
		reports.endTest(loggers);
		reports.flush();

	}

}
