import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ForgotPassword_4A {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		loggers=reports.startTest("Forgot Password-4A");
		
		WebElement forgotPwd=driver.findElement(By.xpath("//a[@id='forgot_password_link']"));
		forgotPwd.click();
		
		if (driver.getCurrentUrl().equals("https://login.salesforce.com/secur/forgotpassword.jsp?locale=us"))
			loggers.log(LogStatus.INFO, "Forgot password page is loaded");
		else
			loggers.log(LogStatus.FAIL, "Forgot password page is not loaded");
		Thread.sleep(2000);
		
		WebElement ForgotUsername=driver.findElement(By.xpath("//input[@id='un']"));
		ForgotUsername.sendKeys("rollyrastogi-nxmg@force.com");
		WebElement continueButton=driver.findElement(By.xpath("//input[@id='continue']"));
		continueButton.click();
		Thread.sleep(2000);
		
		WebElement pwdMsg=driver.findElement(By.xpath("//p[contains(text(),'We�ve sent you an email with a link to finish rese')]"));
		if (pwdMsg.getText().equals("We�ve sent you an email with a link to finish resetting your password."))
			loggers.log(LogStatus.INFO, "Email has been sent to ur mail id");
		else 
			loggers.log(LogStatus.INFO, "email is not send");
		
		
		reports.endTest(loggers);
		reports.flush();

	}

}
