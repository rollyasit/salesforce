import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CheckRemeberMe_3 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);		
	
		 loggers=reports.startTest("Check RemeberMe - 3");

		 WebElement userName3=driver.findElement(By.xpath("//input[@id='username']"));
			userName3.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName3.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord3=driver.findElement(By.xpath("//input[@id='password']"));
		passWord3.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);
		
		WebElement chkbox=driver.findElement(By.xpath("//input[@id='rememberUn']"));
		chkbox.click();
		loggers.log(LogStatus.INFO, "Checked 'Remember me' checked box successfully");
		Thread.sleep(2000);
			
		WebElement logIn3=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn3.click();
		Thread.sleep(2000);
		loggers.log(LogStatus.INFO, "Clicked LogIn button successfully");
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement userProfile=driver.findElement(By.xpath("//span[@id='userNavLabel']"));
		userProfile.click();
		loggers.log(LogStatus.INFO, "Clicked user profile successfully");
		Thread.sleep(5000);
		
		WebElement logOut=driver.findElement(By.xpath("//a[contains(text(),'Logout')]"));
		logOut.click();	
		loggers.log(LogStatus.INFO, "Clicked logout successfully");
		Thread.sleep(5000);
	
		WebElement usernameCheck=driver.findElement(By.xpath("//span[@id='idcard-identity']"));
	
		Thread.sleep(5000);
		if(usernameCheck.getText().equals("rollyrastogi-nxmg@force.com"))
			loggers.log(LogStatus.INFO, "Username is displayed ");
		else
			loggers.log(LogStatus.PASS, "Username is not displayed");
		
		WebElement checkboxCheck=driver.findElement(By.xpath("//input[@id='rememberUn']"));
		if(checkboxCheck.isSelected())
			loggers.log(LogStatus.INFO, "Remember me checkbox is checked");
		else
			loggers.log(LogStatus.FAIL, "Remember me checkbox is not checked");
		
		reports.endTest(loggers);
		reports.flush();

	}

}
