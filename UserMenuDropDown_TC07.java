import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class UserMenuDropDown_TC07 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
				
		WebElement userMenu=driver.findElement(By.xpath("//div[@id='userNavButton']"));
		if (userMenu.isDisplayed())
			loggers.log(LogStatus.INFO, "User menu is available");
		else
			loggers.log(LogStatus.INFO, "User menu is not available");
		
		userMenu.click();
		Thread.sleep(2000);	

		Boolean myProfilefound = false;
		Boolean mySetting = false;
		Boolean devConsole = false;
		Boolean logOut = false;
		
		List<WebElement> dropDownElement = driver.findElements(By.className("mbrMenuItems"));
		for(WebElement value : dropDownElement) 
		{
		   //System.out.println(value.getText());
			if(value.getText().contains("My Profile") )
		   {
		    	myProfilefound=true;
		       // break;
		    }
			if (value.getText().contains("My Settings")) 
				{
				mySetting=true;
				}
			if (value.getText().contains("Developer Console")) 
			{
				devConsole=true;
			}
			if (value.getText().contains("Logout"))
			{
				logOut=true;
			}
		
		}
		
		if(myProfilefound && mySetting && devConsole && logOut) {
		   // System.out.println("Value exists");
		    loggers.log(LogStatus.INFO, "Drop Down with User Profile, My Settings, Developer Console and Logout is displayed successfully");
		}
		else
		{
			 loggers.log(LogStatus.FAIL, "Drop Down with User Profile, My Settings, Developer Console and Logout is not displayed ");	
		}
        
        WebElement mySettings=driver.findElement(By.xpath("//a[contains(text(),'My Settings')]"));
        mySettings.click();
        Thread.sleep(5000);
        
        if (driver.getCurrentUrl().equals("https://na132.salesforce.com/ui/setup/Setup?setupid=PersonalSetup"))
        	loggers.log(LogStatus.INFO, "My Setting page is loaded successfully");
        else
        	loggers.log(LogStatus.INFO, "My Setting page is not loaded successfully");
        
        WebElement personal=driver.findElement(By.xpath("//span[@id='PersonalInfo_font']"));
        personal.click();
        Thread.sleep(5000);
        
        WebElement loginHistroy=driver.findElement(By.xpath("//span[@id='LoginHistory_font']"));
        loginHistroy.click();
        
        WebElement downloadLH=driver.findElement(By.xpath("//a[contains(text(),'Download login history for last six months, includ')]"));
        downloadLH.click();
        Thread.sleep(5000);
        
        File fileDownloaded=new File("C:\\Users\\asita\\Downloads");
        File[] content=fileDownloaded.listFiles();
        
        boolean flagE=false;

        for(int i=0; i<content.length;i++) 
        {
        //	System.out.println(content[i].getName());
        	if((content[i].getName().contains(".csv")))
        	{
        		flagE=true;
        	}
        }
        //System.out.println( flagE);
        
        WebElement displayLayout=driver.findElement(By.xpath("//span[@id='DisplayAndLayout_font']"));
        displayLayout.click();
        
        WebElement customizeTab=driver.findElement(By.xpath("//span[@id='CustomizeTabs_font']"));
        customizeTab.click();
        
        WebElement customApp=driver.findElement(By.xpath("//select[@id='p4']"));
        Select custom=new Select(customApp);
        custom.selectByVisibleText("Salesforce Chatter");
        Thread.sleep(5000);
        
        WebElement availTab=driver.findElement(By.xpath("//select[@id='duel_select_0']"));
        Select tab=new Select(availTab);
        tab.selectByVisibleText("Reports");
        
        WebElement add=driver.findElement(By.xpath("//img[@class='rightArrowIcon']"));
        add.click();
        Thread.sleep(5000);
        
        WebElement selectTab=driver.findElement(By.xpath("//select[@id='duel_select_1']"));
       Select selectTabs=new Select(selectTab);
      // System.out.println(selectTabs.getOptions().size());
       boolean selectedTab=false;
       for(int i=0; i<selectTabs.getOptions().size();i++)
       {
    	   
    	  if (selectTabs.getOptions().get(i).getText().equals("Reports"))
    	  {
    		  selectedTab=true;
    		  break;
    	  }
       }
       if (selectedTab==true)
    	   loggers.log(LogStatus.INFO, "Report Field is added to Selected Tab successfully"); 
       else
    	   loggers.log(LogStatus.INFO, "Report Field is not added to Selected Tab successfully"); 
       
       WebElement emailLink=driver.findElement(By.xpath("//span[@id='EmailSetup_font']"));
       emailLink.click();
      
       WebElement myEmail=driver.findElement(By.xpath("//span[@id='EmailSettings_font']"));
       myEmail.click();
       
       WebElement emailName=driver.findElement(By.xpath("//input[@id='sender_name']"));
       emailName.clear();
       emailName.sendKeys("Bunty Awasthi");
       Thread.sleep(6000);
       
       WebElement emailAdd=driver.findElement(By.xpath("//input[@id='sender_email']"));
       emailAdd.clear();
       emailAdd.sendKeys("rollyrastogi@gmail.com");
       Thread.sleep(6000);
       
       WebElement bcc=driver.findElement(By.xpath("//input[@id='auto_bcc1']"));
       bcc.click();  
       
       WebElement save=driver.findElement(By.xpath("//input[@name='save']"));
       save.click();  
       Thread.sleep(5000);
       try{
    	    Alert alertWindow = driver.switchTo().alert();
     	   Thread.sleep(5000);	
     	   alertWindow.accept();
    	    System.out.println(alertWindow.getText()+" Alert is Displayed"); 
    	    }
       catch(NoAlertPresentException ex){
    	    System.out.println("Alert is NOT Displayed");
    	    }

       
       WebElement calender=driver.findElement(By.xpath("//span[@id='CalendarAndReminders_font']"));
       calender.click();
       Thread.sleep(5000);
       
       WebElement remainders=driver.findElement(By.xpath("//span[@id='Reminders_font']"));
       remainders.click();
       Thread.sleep(5000);
       
       WebElement testRemainder=driver.findElement(By.xpath("//input[@id='testbtn']"));
       testRemainder.click();
       Thread.sleep(5000);
       
       String primaryWindow=driver.getWindowHandle();
       for(String handle:driver.getWindowHandles())
       {
    	   driver.switchTo().window(handle);
       }
     //  WebElement dismiss=driver.findElement(By.xpath("//input[@id='dismiss_all']")) ;
     //  dismiss.click();
     //  Thread.sleep(5000);
       
    //   driver.switchTo().window(primaryWindow);
       
       reports.endTest(loggers);
		reports.flush();
        
	}

}
