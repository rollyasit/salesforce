import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class CreateOpty_TC16 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement opportunity=driver.findElement(By.xpath("//a[contains(text(),'Opportunities')]"));
		opportunity.click();	
		
		if (driver.getCurrentUrl().equals("https://na132.salesforce.com/006/o"))
			loggers.log(LogStatus.INFO, "Opportunity page is loaded successfully");
		else
			loggers.log(LogStatus.INFO, "Opportunity page is not loaded successfully");
		
		WebElement newOportunity=driver.findElement(By.xpath("//div[@id='createNewButton']"));
		newOportunity.click();
		Thread.sleep(5000);	
		
		WebElement createOportunity=driver.findElement(By.xpath("//a[@class='opportunityMru menuButtonMenuLink']"));
		createOportunity.click();
		Thread.sleep(5000);	
		
		WebElement nameOportunity=driver.findElement(By.xpath("//input[@id='opp3']"));
		nameOportunity.clear();
		nameOportunity.sendKeys("Alana");
		Thread.sleep(5000);	
		
		WebElement nameAccty=driver.findElement(By.xpath("//input[@id='opp4']"));
		nameAccty.clear();
		nameAccty.sendKeys("RRg");
		
		WebElement dateCombo=driver.findElement(By.xpath("//input[@id='opp9']"));		
		DateFormat dt=new SimpleDateFormat("MM/dd/yyyy");
		Date dtValue=new Date();
		dateCombo.sendKeys(dt.format(dtValue));
		Thread.sleep(5000);	
		
		WebElement stage=driver.findElement(By.xpath("//select[@id='opp11']"));
		Select stageCombo=new Select(stage);
		stageCombo.selectByVisibleText("Qualification");
		Thread.sleep(5000);	
		
		WebElement prob=driver.findElement(By.xpath("//input[@id='opp12']"));
		prob.clear();
		prob.sendKeys("20");
		Thread.sleep(5000);	
		
		WebElement leadSource=driver.findElement(By.xpath("//select[@id='opp6']"));	
		Select selectLead=new Select(leadSource);
		selectLead.selectByVisibleText("Employee Referral");
		Thread.sleep(5000);	
		
		WebElement savebtn=driver.findElement(By.xpath("//td[@id='bottomButtonRow']//input[@name='save']"));	
		savebtn.click();
		Thread.sleep(5000);	
		
		reports.endTest(loggers);
		reports.flush();
	}

}
