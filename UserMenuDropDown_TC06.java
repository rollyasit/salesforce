import java.awt.AWTException;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class UserMenuDropDown_TC06 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
				
		WebElement userMenu=driver.findElement(By.xpath("//div[@id='userNavButton']"));
		String UserName=userMenu.getText();
		if (userMenu.isDisplayed())
			loggers.log(LogStatus.INFO, "User menu is available");
		else
			loggers.log(LogStatus.INFO, "User menu is not available");
		
		userMenu.click();
		Thread.sleep(2000);
		
		WebElement userProfile=driver.findElement(By.xpath("//a[contains(text(),'My Profile')]"));
		userProfile.click();
		Thread.sleep(5000);
		if (driver.getCurrentUrl().equals("https://na132.salesforce.com/_ui/core/userprofile/UserProfilePage?tab=sfdc.ProfilePlatformFeed"))
			loggers.log(LogStatus.INFO, "User Profile page is loaded successfully");
	/*		
		WebElement editProfile=driver.findElement(By.xpath("//img[@class='recImage user']"));
		editProfile.click();
		Thread.sleep(5000);

		driver.switchTo().frame(driver.findElement(By.id("aboutMeContentId")));
		
		WebElement aboutTab=driver.findElement(By.xpath("//a[contains(text(),'About')]"));
		aboutTab.click();
		Thread.sleep(5000);
		

		WebElement lastName=driver.findElement(By.xpath("//input[@id='lastName']"));
		String updatedLastName="Shukla";
		lastName.clear();
		lastName.sendKeys(updatedLastName);
		Thread.sleep(6000);
				
		WebElement saveAll=driver.findElement(By.xpath("//input[@class='zen-btn zen-primaryBtn zen-pas']"));
		saveAll.click();
		
		driver.switchTo().parentFrame();
		
		WebElement updatedUserName=driver.findElement(By.xpath("//span[@id='tailBreadcrumbNode']"));
		System.out.println( updatedLastName);
		if (updatedUserName.getText().contains(updatedLastName))
			System.out.println(updatedUserName.getText());
	*/		
		WebElement post=driver.findElement(By.xpath("//span[contains(@class,'publisherattachtext')][contains(text(),'Post')]"));
		post.click();
		Thread.sleep(6000);
		
		WebElement postFrame=driver.findElement(By.xpath("//iframe[contains(@title,'Rich Text Editor, publisherRichTextEditor')]"));
		driver.switchTo().frame(postFrame);
		WebElement postText=driver.findElement(By.xpath("/html[1]/body[1]"));
		String postMsg="New Post";
		postText.sendKeys(postMsg);
		driver.switchTo().parentFrame();
		Thread.sleep(6000);
		
		WebElement shareButton=driver.findElement(By.xpath("//input[@id='publishersharebutton']"));
		shareButton.click();
		
		WebElement postMsgDisplay=driver.findElement(By.xpath("//p[contains(text(),'"+ postMsg +"')]"));
		
		if (postMsgDisplay.getText().contains(postMsg))
			loggers.log(LogStatus.INFO, "New post is available");
		else
			loggers.log(LogStatus.INFO, "New post is not available");
		Thread.sleep(5000);
		
		WebElement file= driver.findElement(By.xpath("//span[contains(@class,'publisherattachtext')][contains(text(),'File')]"));
		file.click();
		Thread.sleep(5000);
		
		
		//WebElement uploadComp=driver.findElement(By.id("chatterUploadFileAction"));
		WebElement uploadComp=driver.findElement(By.xpath("//a[@id='chatterUploadFileAction']"));
		uploadComp.click();
		Thread.sleep(5000);
		
		WebElement chooseFile=driver.findElement(By.xpath("//input[@id='chatterFile']"));
		//chooseFile.click();
		chooseFile.sendKeys("C:\\Users\\asita\\Desktop\\TekArch\\test.txt");
		 Thread.sleep(5000);
		
		 // driver.switchTo().activeElement().sendKeys("C:\\Users\\asita\\Desktop\\TekArch\\test.txt");
		//  Thread.sleep(5000);
		/*	
		WebElement uploadComputer=driver.findElement(By.xpath("//a[@id='chatterUploadFileAction']"));
		uploadComputer.click();
		
		WebElement chooseFile =driver.findElement(By.xpath("//input[@id='chatterFile']"));
		chooseFile.click();
		Thread.sleep(5000);
		
	*/	
		//WebElement el=driver.findElement(By.xpath("//input[@id='chatterFile']"));
		//driver.switchTo().activeElement();
		//el.sendKeys("C:\\Users\\asita\\Desktop\\TekArch\\test.txt");
		//Thread.sleep(5000);
		 
		 WebElement photoMouseHover=driver.findElement(By.id("photoSection"));
		 Actions act=new Actions(driver);
		 act.moveToElement(photoMouseHover).build().perform();
		 Thread.sleep(3000);
		 
		 WebElement addPhoto=driver.findElement(By.xpath("//a[@id='uploadLink']"));
		 addPhoto.click();
		 Thread.sleep(4000);
		 
		WebElement postFramePic=driver.findElement(By.xpath("//iframe[@id='uploadPhotoContentId']"));
		driver.switchTo().frame(postFramePic);
		
		 WebElement choosePic=driver.findElement(By.xpath("//input[@id='j_id0:uploadFileForm:uploadInputFile']"));
		 choosePic.sendKeys("C:\\Users\\asita\\Desktop\\TekArch\\image.jpg");
		 
		 Actions crop=new Actions(driver);
		 WebElement cropTracker=driver.findElement(By.xpath("//div[@class='imgCrop_selArea']//div[@class='imgCrop_clickArea']"));
		// crop.clickAndHold(choosePic).moveByOffset(30,280).release().build().perform();
		 crop.dragAndDropBy(cropTracker, 30,220).perform();
		 Thread.sleep(5000);
		 
		 WebElement savePic=driver.findElement(By.xpath("//input[@id='j_id0:j_id7:save']"));
		 savePic.click();
		 driver.switchTo().parentFrame();
		 Thread.sleep(5000);
			reports.endTest(loggers);
			 reports.flush();
		
	}

}
