import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Leads_TC21 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("User Menu Drop Down-TC05");

		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
			userName.sendKeys("rollyrastogi-nxmg@force.com");
			if (userName.getText()!=null)
				loggers.log(LogStatus.INFO, "Username enetered successfully");		
			else
				loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		passWord.sendKeys("rolly123");	
		loggers.log(LogStatus.INFO, "Entered password successfully");
		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		loggers.log(LogStatus.INFO, "Clicked LogIn button");
		Thread.sleep(2000);	
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
		
		WebElement leads=driver.findElement(By.xpath("//a[contains(text(),'Leads')]"));
		leads.click();
		Thread.sleep(5000);	
		
		if(driver.getCurrentUrl().contains("https://na132.salesforce.com/00Q/o"))
			loggers.log(LogStatus.INFO, "Lead home page is opened successfully");
		else
			loggers.log(LogStatus.FAIL, "Lead home page is not opened successfully");
		
		WebElement view=driver.findElement(By.xpath("//select[@id='fcf']"));
		view.click();
		
		boolean allOplead=false;
		boolean myUnread=false;
		boolean recentView=false;
		boolean todayLead=false;
		
		List <WebElement> viewList=driver.findElements(By.xpath("//select[@id='fcf']"));
		for(WebElement value:viewList) 
		{
			if(value.getText().contains("All Open Leads") )
			   {
			    	allOplead=true;
			    }
				if (value.getText().contains("My Unread Leads")) 
					{
					myUnread=true;
					}
				if (value.getText().contains("Recently Viewed Leads")) 
				{
					recentView=true;
				}
				if (value.getText().contains("Today's Leads"))
				{
					todayLead=true;
				}
		}
		
		if(allOplead && myUnread && recentView && todayLead )
		    loggers.log(LogStatus.INFO, "Drop down with All Open Leads, My Unread Leads, Recently Viewed Leads, Today's Lead is loaded successfully");			
		else
		    loggers.log(LogStatus.INFO, "Drop down with All Open Leads, My Unread Leads, Recently Viewed Leads, Today's Lead is not loaded successfully");
		
		reports.endTest(loggers);
		reports.flush();

	}

}
