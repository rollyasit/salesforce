import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class LoginErrorMessage_1 {
	static ExtentReports reports;
	static ExtentTest loggers;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		
		String filename=new SimpleDateFormat("'sample demo_'yyyyMMddHHmm'.html'").format(new Date());		
		String path="C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\"+filename;
		reports=new ExtentReports(path);
		
		loggers=reports.startTest("Login Error Message - 1");
		
		WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
		userName.sendKeys("rollyrastogi-nxmg@force.com");
		if (userName.getText()!=null)
			loggers.log(LogStatus.INFO, "Username enetered successfully");		
		else
			loggers.log(LogStatus.INFO, "Username is not enetered successfully");
		
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		Thread.sleep(2000);
		passWord.clear();
		
		if (passWord.getText()==null)
			loggers.log(LogStatus.INFO, "Password field is cleared successfully.");		
		else
			loggers.log(LogStatus.INFO, "Password field is not cleared.");
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		logIn.click();
		
		WebElement errorMsg1=driver.findElement(By.xpath("//div[@id='error']"));
		
		 if (errorMsg1.getText().equals("Please enter your password."))
			 loggers.log(LogStatus.PASS, "Test: Navigate to SFDC is passed.");
		 else
			 loggers.log(LogStatus.FAIL, "Test: Navigate to SFDC is failed.");		
		
		reports.endTest(loggers);
		 reports.flush();
		 		 

	}

}
